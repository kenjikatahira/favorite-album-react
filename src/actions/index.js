
import { getUserTopAlbums, searchAlbums } from '../apis/lastfm';

export const getUserAlbum = (query) => {
    return dispatch => {
        getUserTopAlbums(query).then((response) => {
            dispatch({
                type : 'USER_ALBUMS',
                payload : response.data.topalbums.album
            });
        })
    }
}

export const getAlbums = (query) => {
    return dispatch => {
        searchAlbums(query).then((response) => {
            dispatch({
                type : 'SEARCH_ALBUMS',
                payload : response.data.results.albummatches.album
            });
        })
    }
}

export const searchLastFm = (query) => {
    return dispatch => {
        searchAlbums(query).then((response) => {
            dispatch({
                type : 'SEARCH_INPUT',
                payload : response.data.results.albummatches.album
            });
        })
    }
}

export const favorite = (album) => {
    return {
        type : 'FAVORITE',
        payload : album
    };
}

export const unfavorite = (album) => {
    return {
        type : 'UNFAVORITE',
        payload : album
    };
}