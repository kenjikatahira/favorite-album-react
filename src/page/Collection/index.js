import React, { Component } from 'react';
import { connect } from 'react-redux';
import { searchLastFm } from '../../actions';
import Card from '../../components/Card';
import './collection.css';

class Collection extends Component {
    constructor(props) {
        super(props);
        this.props.searchLastFm(this.props.search);
    }
    hasFavorite(album) {
        this.props.favorites.forEach(item => {
            if(item.mbid === album.mbid) {
                return true;
            }
        });
        return false;
    }
    /*
    metodo renderiza os componentes cards
    */
   renderCard = (album,index) => {
        if(album.mbid) {
            return (
                <div key={index}>
                    <Card album={album} favorited={this.hasFavorite(album)}/>
                </div>
            )
        }
    }
    render(){
        const search = this.props.search;
        if(!search) {
            return <div>Loading...</div>
        } 
        return (
            <div className="collection ui container">
                <div className="ui three column padded centered grid">
                    {search.map(this.renderCard)}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user : state.user,
        search : state.search,
        favorites : state.favorites
    }
}

export default connect(mapStateToProps, {
    searchLastFm,
})(Collection);
