import React, { Component } from 'react';
import { connect } from 'react-redux';
import Card from '../../components/Card';
import { getUserAlbum } from '../../actions';

class Home extends Component {
    constructor(props) {
        super(props);
        // dispara a action para requisição dos dados
        this.props.getUserAlbum({
            user: this.props.user.name
        });
    }
    /*
    metodo renderiza os componentes cards
    */
    renderTopAlbums(album,index) {
        if(album.mbid) {
            return (
                <div key={index} >
                    <Card album={album} />
                </div>
            )
        }
    }
    render() {
        // albums da requisição
        const albums = this.props.user.albums;
        if(!albums) {
            return <div>Loading...</div>
        } 
        return (
            <div className="collection ui container">
                    <h2>Most listen albums</h2>
                <div className="ui three column padded centered grid">
                    {albums.map(this.renderTopAlbums)}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user : {
            name : state.user,
            albums : state.albums
        }
    }
}

export default connect(mapStateToProps, { getUserAlbum })(Home);