import React, { Component } from 'react';
import { getAlbumInfo } from '../../apis/lastfm';
import TableAlbum from '../../components/TableAlbum';
import './Album.css';


class Album extends Component {
    constructor(props) {
        super(props);
        this.state = { album : null, tracks : null };
    }
    componentDidMount() {
        // Parametros da busca
        const params = this.getAlbumParams();
        // Repassa os parametros para a request
        getAlbumInfo(params).then((response)=>{
            this.setState({
                album : response.data.album,
                tracks : response.data.album.tracks.track
            });
        });
        // scroll to top
        window.scroll(0,document.innerHeight);
    }
    /**
     * Pega e atribui os parametros necessários para a busca 
     */
    getAlbumParams() {
        if(this.props.location.state && this.props.location.state.artist) {
            // url acessada por um link
            return this.props.location.state;
        } else {
            // url acessada diretamente
            const [artist,albumName] = this.props.location.pathname.replace('/album/','').replace(/-/g,' ').split('/');
            return { artist, albumName };
        }
    }
    render(){
        if(!this.state.album) {
            return <h2 className="ui header">loading...</h2>;
        }

        return (
            <div className="album ui container">
                <div className="content">
                    <div className="ui grid">
                        <div className="album-info four wide column">

                            <div className="ui piled segments">
                                <div className="ui segment text-center">
                                    <p className="ui"><b>{this.state.album.artist}</b></p>
                                </div>
                                <div className="ui segment text-center">
                                    <p className="ui">{this.state.album.name}</p>
                                </div>
                            </div>
                            <div className="info-image">
                                <img className="ui fluid image" src={this.state.album.image[2]['#text']} alt={`${this.state.album.artist} - ${this.state.album.name}`}/>
                            </div>

                        </div>
                        <div className="album-list twelve wide column">
                            <TableAlbum album={this.state.album}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Album;