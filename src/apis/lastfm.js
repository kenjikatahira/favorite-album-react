import { LASTFMKEY } from '../constants';
import axios from 'axios';

const random = () => {
    const a = ['colorado','alice','chains','boogarins','watson'];
    return a[Math.round(Math.random() * (1 * a.length))]
};

export const searchAlbums = ({method='album.search',query=random(),limit=50}) => {
    return axios({
        method: 'get',
        baseURL : 'http://ws.audioscrobbler.com/2.0/',
        params : {
            method : method,
            album : query,
            limit : limit,
            api_key : LASTFMKEY,
            format : 'json'
        }
    })
}

export const getAlbumInfo = ({artist,albumName}) => {
    return axios({
        method: 'get',
        baseURL : 'http://ws.audioscrobbler.com/2.0/',
        params : {
            method : 'album.getinfo',
            artist : artist,
            album : albumName,
            api_key : LASTFMKEY,
            format : 'json'
        }
    })
}

export const getUserTopAlbums = ({user,limit = 6}) => {
    return axios({
        method: 'get',
        baseURL : 'http://ws.audioscrobbler.com/2.0/',
        params : {
            method : 'user.gettopalbums',
            user : user,
            limit : limit,
            api_key : LASTFMKEY,
            format : 'json'
        }
    })
}
