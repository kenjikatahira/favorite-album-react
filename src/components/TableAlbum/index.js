import React from 'react';

const TableAlbum = (props) => {
    const formatDuration = (item) => {
        const duration = item.split('');
        if(duration.length === 3) {
            return `${duration[0]}:${duration[1]}${duration[2]}`
        } else if(duration.length === 4) {
            return `${duration[0]}${duration[1]}:${duration[2]}${duration[3]}`
        } else if(duration.length === 2) {
            return `${duration[0]}${duration[1]}s` 
        }
    };
    const renderItems = (track) => {
        return (
            <tr key={`${track.name}-${track.duration}`}>
                <td>
                    <i className="music icon">
                    </i>
                    <span>
                        {track.name}
                    </span>
                </td>
                <td className="right aligned collapsing">
                    {formatDuration(track.duration)}
                </td>
            </tr>
        )
    }
    const tracks = props.album.tracks.track;
    if( !tracks || tracks.length === 0) {
        return (<tr><td>Não há uma lista de musica</td></tr>)
    }

    return(
        <table className="ui celled striped table">
            <thead>
                <tr><th colSpan="2">
                    Album | {props.album.artist} - {props.album.name}
                </th>
            </tr></thead>
            <tbody>
                {tracks.map(renderItems)}
            </tbody>
        </table>
    )
}

export default TableAlbum;