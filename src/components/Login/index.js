import React, { Component } from 'react';
import { LASTFMKEY } from '../../constants';
import Axios from 'axios';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token : ''
        }
    }
    getData (){
        Axios({
            baseURL : 'http://www.last.fm/api/auth/',
            method: 'GET',
            crossDomain: true,
            'Access-Control-Allow-Origin' : '*',
            params : {
                token : this.state.token,
                api_key : LASTFMKEY
            }
        }).then(data => {
            console.log(data);
        })
    }
    componentDidMount() {
        this.getToken();
    }
    getToken() {  
        if( !this.props.location.search ) return;
        this.setState({ token : this.props.location.search.replace('?token=','') })
        this.getData();
    }
    render () {
        console.log(this.state)
        return (
            <div>
                <a href={`http://www.last.fm/api/auth/?api_key=${LASTFMKEY}&cb=http://localhost:3000/login`}>Redirect</a>
            </div>
        )
    }
}

export default Login;
