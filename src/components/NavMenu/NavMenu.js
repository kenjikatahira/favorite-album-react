import React, { Component } from 'react';

class NavMenu extends Component {
    render(){
        return(
            <div className="ui attached stackable menu">
                <div className="ui container">
                    <Link className="item" to="/home">
                        <i className="home icon"></i> Home
                            </Link>
                    <Link className="item" to="/browse">
                        <i className="grid layout icon"></i> Albums
                            </Link>
                    <div className="right item">
                        <div className="ui right action left icon input">
                            <i className="search icon"></i>
                            <input type="text" placeholder="Search" value={this.state.input} onKeyDown={this.keyPress} onChange={this.getInputValue} />
                            <div className="ui basic floating button" onClick={this.searchBind}>
                                submit
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}