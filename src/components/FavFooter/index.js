import React from 'react';
import { connect } from 'react-redux';
import { unfavorite } from '../../actions';
import './FavFooter.css';

class FavFooter extends React.Component {
    constructor(props){
        super(props);
    }
    onUnfavorite = (event) => {
        this.props.favorites.filter((item) => {
            if (item.mbid === event.target.id ) {
                this.props.unfavorite(item);
                item.favorited = false;
                return item;
            };
            return false;
        });
    }
    renderFooter = (fav) => {
        return (
            <div className="fav swing-in-left-bck" key={fav.name}>
                <i class="window close icon" id={fav.mbid} onClick={this.onUnfavorite}></i>
                <img className="ui middle aligned small image" src={fav.image[2]['#text']} alt={fav.name} />
            </div>
        )
    }
    footerClasses(){
        if(this.props.favorites.length === 0) {
            return 'ui hide fav-footer';
        }

        return 'ui fav-footer';
    }
    render() {
        return (
            <div className={this.footerClasses()}>
                {this.props.favorites.map(this.renderFooter)}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        favorites : state.favorites
    }
}

export default connect(mapStateToProps, {
    unfavorite
})(FavFooter);
