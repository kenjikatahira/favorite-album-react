import React from 'react';
import { Link } from 'react-router-dom';
import slug from 'slug';

const CardDetail = (props) => {
    let artist = '';

    if( typeof props.album.artist === 'string') {
        artist = props.album.artist
    } else {
        artist = props.album.artist.name;
    }

    const link = {
        pathname: `/artist/${slug(artist).toLowerCase()}`,
        state: {
            artist : artist,
            albumName : props.album.name
        }
    }

    return (
        <div className="content">
            <Link to={link} className="header">
                {artist}
            </Link>
            <div className="description">
                {props.album.name}
            </div>
        </div>
    )
}

export default CardDetail;