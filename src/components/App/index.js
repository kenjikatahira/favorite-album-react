import React from 'react';
import { BrowserRouter , Link, Route, Switch} from 'react-router-dom';
import { connect } from 'react-redux';

// import Home from '../../page/Home';
import Collection from '../../page/Collection';
import Album from '../../page/Album';
import FavFooter from '../FavFooter';
import Login from '../Login';

import './App.css';

import { searchLastFm } from '../../actions';

class App extends React.Component {
    state = {
        title : 'Favorite Album',
        input : ''
    }
    getInputValue = (event) => {
        this.setState({
            input : event.target.value
        });
    }
    searchBind = () => {
        this.props.searchLastFm({
            method : 'album.search',
            query : this.state.input,
            limit : 42
        });
    }
    keyPress = (e) => {
        if(e.keyCode === 13){
            this.searchBind();
         }
    }
    render() {
        return (
            <div className="app">
                <BrowserRouter>
                    <div className="ui secondary pointing menu">
                        <div className="ui container">
                        <p className="item title">{this.state.title}</p>
                            {/* <Link className="item" to="/home">
                                <i className="home icon"></i> Home
                            </Link> */}
                            <Link className="item" to="/">
                                <i className="grid layout icon"></i> Albums
                            </Link>
                            <div className="right item">
                                <div className="ui right action left icon input shake">
                                    <i className="search icon"></i>
                                    <input type="text" placeholder="Search" value={this.state.input} onKeyDown={this.keyPress} onChange={this.getInputValue}/>
                                    <div className="ui basic floating button" onClick={this.searchBind}>
                                        search
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Switch>
                        {/* <Route exact path='/' component={ Home } /> */}
                        <Route exact path='/' component={ Collection } />
                        <Route path='/album' component={ Album } />
                        <Route exact path='/login' component={ Login } />
                    </Switch>
                </BrowserRouter>
                <FavFooter />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user : state.user,
        search : state.search
    }
}

export default connect(mapStateToProps,{searchLastFm})(App); 
