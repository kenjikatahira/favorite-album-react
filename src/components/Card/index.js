import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import CardDetail from '../CardDetail';
import Maps from '../Maps';
import { favorite,unfavorite } from '../../actions';
import slug from 'slug';
import './Card.css';

class Card extends React.Component {
    link = {
        pathname: `/album/${slug(this.props.album.artist.name || this.props.album.artist).toLocaleLowerCase()}/${slug(this.props.album.name.toLowerCase())}`,
        state: {
            artist : this.props.album.artist.name || this.props.album.artist,
            albumName : this.props.album.name
        }
    }
    onFavorite = (event) => {
        this.props.albums.forEach(item=>{
            if (event.target.id === item.mbid && !item.favorited && !this.props.favorites.includes(item)) {
                this.props.favorite(item);
            }
        })
    }
    hasFavorite() {
        // verifica se o album está nos favoritos
        if(this.props.favorites.includes(this.props.album)) {
            return 'heart red like icon';
        }
        return 'heart like outline icon';
    }
    render() {
        const image = this.props.album.image['3']['#text'];

        return (
            <div className="ui card">
                <div className="image">
                    <Link to={this.link} className="header">
                        <img className="ui image" src={image} alt={this.props.album.name} />
                    </Link>
                </div>
                <CardDetail album={this.props.album}/>
                <div className="extra content">
                    <i className={this.hasFavorite()} id={this.props.album.mbid} onClick={this.onFavorite}></i>
                </div>
                <Maps />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user : state.name,
        albums : state.search,
        favorites : state.favorites
    }
}

export default connect(mapStateToProps, {
    favorite,
    unfavorite
})(Card);
