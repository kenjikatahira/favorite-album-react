import { combineReducers } from 'redux';

const search = (state = [],action) => {
    if(action.type === 'SEARCH_ALBUMS') {
        return [...action.payload]
    } else if( action.type === 'SEARCH_INPUT') {
        return action.payload
    } 

    return state;
}

const albums = (state = [],action) => {
    if(action.type === 'USER_ALBUMS') {
        return action.payload;
    }
    return state;
}

const user = (user = 'kenjicas',action) => {
    return user;
}

const favorites = (favorites = [],action) => {
    if(action.type === 'FAVORITE') {
    return [...favorites,action.payload];
    } else if (action.type === 'UNFAVORITE') {
        return favorites.filter(album => album.mbid !== action.payload.mbid);
    }

    return favorites;
}

export default combineReducers({
    favorites,
    search,
    albums,
    user
});
